package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/go-stomp/stomp"
	"github.com/go-stomp/stomp/frame"

	"github.com/rs/xid"

	"bitbucket.org/lucas226/common/pkg/log"
	"bitbucket.org/lucas226/common/pkg/mq"
	"bitbucket.org/lucas226/common/pkg/writer"
	"github.com/fatih/color"
	"github.com/urfave/cli"
)

type SubscriptionConfig struct {
	Topic   string
	Headers map[string]string
	Durable bool
}

type Config struct {
	Env           string
	Host          string
	User          string
	Password      string
	Subscriptions []SubscriptionConfig
	ClientID      string `json:"client_id,omitempty"`

	Writer *writer.Config
}

var config = Config{Env: "dev"}
var configPath string
var topic string

func main() {
	app := cli.NewApp()
	app.Name = "stomple"
	app.Usage = "a stomp cli tool"
	app.Version = "0.0.1"

	cli.HelpFlag = cli.BoolFlag{
		Name:  "help",
		Usage: "show help",
	}

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "environment, either 'dev' or 'prod'",
			Destination: &config.Env,
		},
		cli.StringFlag{
			Name:        "host, h",
			Usage:       "server address for message queue",
			Destination: &config.Host,
		},
		cli.StringFlag{
			Name:        "user, u",
			Usage:       "username",
			Destination: &config.User,
		},
		cli.StringFlag{
			Name:        "password, p",
			Usage:       "password",
			Destination: &config.Password,
		},
		cli.StringFlag{
			Name:        "topic, t",
			Usage:       "topic names to subscribe, use , to separate if there are multiple",
			Destination: &topic,
		},
		cli.StringFlag{
			Name:        "clientid, i",
			Usage:       "topic names to subscribe, use , to separate if there are multiple",
			Destination: &config.ClientID,
		},
		cli.StringFlag{
			Name:        "config, c",
			Usage:       "path of config file. If config is specified, all other command line flags are ignored",
			Destination: &configPath,
		},
	}

	app.Commands = []cli.Command{
		{
			Name:   "sub",
			Usage:  "subscribe to a topic or queue",
			Action: subCmd,
		},
		{
			Name:   "stream",
			Usage:  "log to a topic or queue",
			Action: streamCmd,
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Panic("stomple died", "error", err)
	}
}

func prepare() {
	if configPath != "" {
		configFile, err := os.Open(configPath)
		// if we os.Open returns an error then handle it
		if err != nil {
			panic("cannot open config file: " + err.Error())
		}
		// defer the closing of our configFile so that we can parse it later on
		defer configFile.Close()

		byteValue, err := ioutil.ReadAll(configFile)
		if err != nil {
			panic("cannot read config file" + err.Error())
		}
		json.Unmarshal([]byte(byteValue), &config)
	} else {
		topics := strings.Split(topic, ";")
		for _, t := range topics {
			config.Subscriptions = append(config.Subscriptions, SubscriptionConfig{t, nil, false})
		}
	}
	log.InitLogger(log.LoggerConfig{Env: config.Env})
	log.Info("config loaded", "path", configPath)
}

func connect() *chan *stomp.Message {
	var subs = make(chan *stomp.Message, 1000)

	if config.ClientID == "" {
		config.ClientID = "[stomple]" + xid.New().String()
	}
	log.Info("connection started --------------------------", "clientID", config.ClientID)
	conn := mq.NewMQConnector(config.ClientID, config.Host, config.User, config.Password, 5*time.Second)
	log.Info("subscription started ---------------------------")
	for _, sub := range config.Subscriptions {
		var s *stomp.Subscription
		var err error
		headers := []func(*frame.Frame) error{}
		for k, v := range sub.Headers {
			headers = append(headers, stomp.SubscribeOpt.Header(k, v))
		}
		if sub.Durable {
			s, err = conn.DurableSubscribe(sub.Topic, stomp.AckAuto, headers...)
		} else {
			s, err = conn.Subscribe(sub.Topic, headers...)
		}
		if err != nil {
			log.Panic("cannot subscribe")
		}

		go func() {
			for msg := range s.C {
				subs <- msg
			}
		}()
		log.Info("subscription successful", "subscription", sub)
	}
	log.Info("subscription completed ------------------------")
	return &subs
}

func subCmd(c *cli.Context) error {
	prepare()
	colorBoard := []*color.Color{color.New(color.FgBlue), color.New(color.FgCyan), color.New(color.FgGreen), color.New(color.FgMagenta)}
	colorBoardSize := len(colorBoard)
	channels := connect()
	for msg := range *channels {
		if msg.Err != nil {
			return msg.Err
		}

		fmt.Print("|")
		for i := 1; i < msg.Header.Len(); i++ {
			k, v := msg.Header.GetAt(i)
			colorBoard[i%colorBoardSize].Printf("%s[%s]", k, v)
			fmt.Print("|")
		}
		fmt.Printf("\n")
		fmt.Printf("%s\n", msg.Body)
	}
	return nil
}

func streamCmd(c *cli.Context) error {
	prepare()
	channels := connect()
	w, err := writer.NewWriter(config.Writer)
	if err != nil {
		return err
	}
	for msg := range *channels {
		if msg.Err != nil {
			return msg.Err
		}

		var obj = map[string]interface{}{}
		for i := 1; i < msg.Header.Len(); i++ {
			k, v := msg.Header.GetAt(i)
			obj[k] = v
		}
		obj["data"] = json.RawMessage(msg.Body)

		w.Write(obj)
	}
	return nil
}
