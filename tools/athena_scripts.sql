
-- create dev mis table
CREATE EXTERNAL TABLE `mis`(
  `content-type` string COMMENT 'from deserializer', 
  `data` string COMMENT 'from deserializer', 
  `destination` string COMMENT 'from deserializer', 
  `expires` bigint COMMENT 'from deserializer', 
  `message-id` string COMMENT 'from deserializer', 
  `priority` int COMMENT 'from deserializer', 
  `subscription` int COMMENT 'from deserializer', 
  `timestamp` timestamp COMMENT 'from deserializer')
PARTITIONED BY ( 
  `y` int, 
  `m` int, 
  `d` int)
ROW FORMAT SERDE 
  'org.openx.data.jsonserde.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat'
LOCATION
  's3://tt-dev-mq-archive/mis'
TBLPROPERTIES (
  'has_encrypted_data'='false', 
  'transient_lastDdlTime'='1550615721')