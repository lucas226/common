#!/bin/sh

## get config from secret manageer
aws secretsmanager get-secret-value --secret-id $SECRET_ID --region us-west-2 | jq -r .SecretString > config.json
echo "successfully get config from $SECRET_ID"
echo "exec $@"

# Hand off to the CMD
exec "$@"
