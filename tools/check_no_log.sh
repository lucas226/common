#!/usr/bin/env bash
#######################################################################
# Check go file
#######################################################################

test -z "$(find . -path "./vendor" -prune -o -type f -name '*.go' -type f -not -name '*_test.go' -print | xargs grep --line-number 'log/.'  | tee /dev/stderr )"
if [ $? -eq 1 ] ;then
  echo "[FAIL] There are log function. You must replace log to logger (zap)."
  exit 1
else
  echo "[PASS] log check ok"
  exit 0
fi
