FROM python:3.7-alpine

WORKDIR /root/

# RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

COPY ./out/stomple /root/stomple
COPY ./tools/ecs_entrypoint.sh /root/
RUN chmod 700 /root/ecs_entrypoint.sh
RUN apk update
RUN apk add jq
RUN pip3 install awscli

ENTRYPOINT ["/root/ecs_entrypoint.sh"]
CMD ["/root/stomple", "-c", "/root/config.json", "stream"]