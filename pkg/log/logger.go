package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var sugaredLogger *zap.SugaredLogger

var Info func(msg string, keysAndValues ...interface{}) = nil
var Debug func(msg string, keysAndValues ...interface{}) = nil
var Warn func(msg string, keysAndValues ...interface{}) = nil
var Error func(msg string, keysAndValues ...interface{}) = nil
var Panic func(msg string, keysAndValues ...interface{}) = nil

// LoggerConfig stores config for logger
type LoggerConfig struct {
	Env string
}

// InitLogger should be called first
func InitLogger(config LoggerConfig) {
	if sugaredLogger != nil {
		Error("logger already initialized")
		return
	}

	var zapConfig zap.Config
	switch config.Env {
	case "dev":
		zapConfig = zap.NewDevelopmentConfig()
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	case "prod":
		zapConfig = zap.NewProductionConfig()
	default:
		panic("logger env should be either dev or prod: " + config.Env)
	}

	logger, err := zapConfig.Build()
	if err != nil {
		panic("cannot build logger")
	}
	sugaredLogger = logger.Sugar()
	Info = sugaredLogger.Infow
	Debug = sugaredLogger.Debugw
	Warn = sugaredLogger.Warnw
	Error = sugaredLogger.Errorw
	Panic = sugaredLogger.Panicw

	Info("logger initialized", "config", config)
}
