package configs

import (
	"fmt"

	"bitbucket.org/lucas226/common/pkg/messenger"
	"github.com/juju/errors"
	"github.com/spf13/viper"
)

/**********************************************************************
 * MIS(Market Info Service) Configuration: for mario and oaas configs *
 **********************************************************************/

type MISConfig struct {
	Env            string                      `mapstructure:"env"`
	Platform       string                      `mapstructure:"platform"` // usually it's exchange name, but can be others like coinapi
	Addr           string                      `mapstructure:"addr"`
	LinkerMaximum  int                         `mapstructure:"linker_maximum"`
	Timeout        int                         `mapstructure:"timeout"`
	MqEndpoint     string                      `mapstructure:"mq_endpoint"`
	MqUsername     string                      `mapstructure:"mq_username"`
	MQPassword     string                      `mapstructure:"mq_password"`
	Linkers        []LinkerConfig              `mapstructure:"linkers"`
	OrderbookLevel int                         `mapstructure:"orderbook_level,omitempty"` // default value is 20 if empty
	APIKey         string                      `mapstructure:"api_key,omitempty"`         // only applicable for coinApi
	Messengers     []messenger.MessengerConfig `mapstructure:"messenger"`
}

type LinkerConfig struct {
	ID      int             `mapstructure:"id"`
	Timeout int             `mapstructure:"timeout"`
	Pairs   []MISPairConfig `mapstructure:"pairs"`
}

type MISPairConfig struct {
	//Exchange  string `json:"exchange,omitempty"` // only applicable for coinApi
	Symbol         Symbol `mapstructure:"symbol"`
	Trade          bool   `mapstructure:"trade"`
	Orderbook      bool   `mapstructure:"orderbook"`
	OrderbookLevel int    `mapstructure:"orderbook_level"`
}

func LoadMISConfig(path string) (*MISConfig, error) {
	cfg := MISConfig{OrderbookLevel: 20} //default orderbook parsing level
	//viper.AddConfigPath(path)  // call multiple times to add many search paths
	viper.SetConfigType("json")
	// viper.SetEnvPrefix("mis")
	// viper.AutomaticEnv()
	viper.SetConfigFile(path)
	// environment variables
	// viper.BindEnv("mq_username")
	// viper.BindEnv("mq_password")
	// viper.BindEnv("mq_endpoint")
	// viper.BindEnv("api_key")
	// viper.BindEnv("chat_url")
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.New(fmt.Sprintf("read configs file error:%v", err))
	} else {
		if err := viper.Unmarshal(&cfg); err != nil {
			return nil, errors.New(fmt.Sprintf("unmarshal configs data error:%v", err))
		} else {
			return &cfg, nil
		}
	}
}
