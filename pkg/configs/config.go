package configs

/*
	common structures that possibly be used in any type of configs
*/
type DeliveryTime = string
type SecurityType = string

type Symbol struct {
	Base         string       `json:"base"`
	Quote        string       `json:"quote"`
	DeliveryTime DeliveryTime `json:"deliveryTime,omitempty"` // default is 'empty'
	SecurityType SecurityType `json:"securityType,omitempty"` // default is 'SPOT'
	Strike       string       `json:"strike,omitempty"`
	Side         string       `json:"side,omitempty"`
}
