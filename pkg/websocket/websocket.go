package websocket

import (
	"time"

	"bitbucket.org/lucas226/common/pkg/commons"
	"bitbucket.org/lucas226/common/pkg/log"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
)

// Conn holds websocket connection
type Conn struct {
	conn         *websocket.Conn
	url          string
	IncomingChan chan []byte
}

// NewConn  Return a websocket Conn Instance
func NewConn(url string) (*Conn, error) {
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, errors.Wrap(err, "New Ws Connection failed")
	}
	inChan := make(chan []byte, 1000)
	return &Conn{
		conn:         conn,
		url:          url,
		IncomingChan: inChan,
	}, nil
}

// Start Receive data from websocket read and send to Luigi through processChannel
// Process: 1. Start a new go routine
// 		 	2. read websocket message and put into channel
func (c *Conn) Start() {
	for {
		t, msg, err := c.conn.ReadMessage()
		if err != nil {
			log.Error("websocket read message error", "error", err)
			return
		}
		switch t {
		case websocket.TextMessage, websocket.BinaryMessage:
			if err := commons.SafeSendBytesChan(c.IncomingChan, msg); err != nil {
				log.Error("send message to a closed chan", "error", err)
				return
			}
		case websocket.PingMessage:
			log.Debug("websocket receive ping message")
		case websocket.PongMessage:
			log.Debug("websocket receive pong message")
		case websocket.CloseMessage:
			log.Info("websocket receive close message")
			c.Close()
			return
		}
	}
}

// WriteJSON into websocket
func (c *Conn) WriteJSON(v interface{}) error {
	err := c.conn.WriteJSON(v)
	if err != nil {
		return errors.Wrap(err, "ws can not send")
	}
	return nil
}

// Read message from
func (c *Conn) Read() (messageType int, p []byte, err error) {
	messageType, p, err = c.conn.ReadMessage()
	if err != nil {
		err = errors.Wrap(err, "read message error")
	}
	return
}

// Write data into websocket
func (c *Conn) Write(messageType int, data []byte) error {
	err := c.conn.WriteMessage(messageType, data)
	if err != nil {
		return errors.Wrap(err, "ws can not write")
	}
	return nil
}

// Close websocket connection
func (c *Conn) Close() {
	log.Info("closing websocket")
	err := c.conn.Close()
	if err != nil {
		log.Error("close websocket error", "error", err)
	}
	commons.SafeCloseBytesChan(c.IncomingChan)
	log.Info("websocket closed")
}

// SetPongHandler sets pong handler
func (c *Conn) SetPongHandler(h func(appData string) error) {
	c.conn.SetPongHandler(h)
}

// WritePing to websocket
func (c *Conn) WritePing() error {
	return c.conn.WriteControl(websocket.PingMessage, nil, time.Time{})
}
