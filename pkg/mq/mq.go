package mq

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"regexp"
	"time"

	"bitbucket.org/lucas226/common/pkg/constants"
	"bitbucket.org/lucas226/common/pkg/log"
	"github.com/go-stomp/stomp"
	"github.com/go-stomp/stomp/frame"
	"github.com/pkg/errors"
)

var validURL = regexp.MustCompile(`^stomp(\+ssl)?:\/\/(.+):(\d+)$`)

// NewMQConnector create a new MQConnector instance
func NewMQConnector(clientID string, endpoint string, username string, password string, heartBeatTime time.Duration) *MQConnector {
	connector := &MQConnector{
		clientID:      clientID,
		endpoint:      endpoint,
		username:      username,
		password:      password,
		subscriptions: make(map[string]int),
		heartBeatTime: heartBeatTime,
		conn:          nil,
	}
	if err := connector.connect(heartBeatTime); err != nil {
		log.Panic("failed to create new connector", "error", err)
	}
	return connector
}

// MQConnector is a stomp mq connector
type MQConnector struct {
	endpoint      string
	username      string
	password      string
	conn          *stomp.Conn
	clientID      string
	subscriptions map[string]int // store all subscribed topics: topic_string->subscription
	heartBeatTime time.Duration
}

// private function, connect to message queue
func (connector *MQConnector) connect(heartBeatTime time.Duration) error {
	submatch := validURL.FindStringSubmatch(connector.endpoint)
	if len(submatch) != 4 {
		log.Error("url not supported", "endpoint", connector.endpoint, "submatch", submatch)
		return errors.New("mq url not supported: " + connector.endpoint)
	}
	host := submatch[2]
	port := submatch[3]

	// setup connection
	tcpConn, err := net.DialTimeout("tcp", host+":"+port, 10*time.Second)
	if err != nil {
		return err
	}
	var netConn io.ReadWriteCloser = tcpConn
	if submatch[1] == "+ssl" {
		sslConn := tls.Client(tcpConn, &tls.Config{ServerName: host})
		if err := sslConn.Handshake(); err != nil {
			log.Error("ssl handshake failed", "error", err)
			return errors.Wrap(err, "ssl handshake failed")
		}
		netConn = sslConn
	}

	options := []func(*stomp.Conn) error{
		stomp.ConnOpt.HeartBeat(heartBeatTime, heartBeatTime),
		stomp.ConnOpt.HeartBeatError(2 * heartBeatTime),
		stomp.ConnOpt.Login(connector.username, connector.password),
		stomp.ConnOpt.Header("client-id", connector.clientID),
	}
	conn, err := stomp.Connect(netConn, options...)
	if err != nil {
		log.Error("stomp connect failed", "error", err)
		return errors.Wrap(err, "stomp connect failed")
	}
	connector.conn = conn
	log.Info("connected to ActiveMQ", "endpoint", connector.endpoint)
	return nil
}

// private function, disconnect to message queue
func (connector *MQConnector) disconnect() error {
	if err := connector.conn.Disconnect(); err != nil {
		log.Error("disconnect failed", "error", err)
		return errors.Wrap(err, "disconnect failed")
	}
	log.Info("mq_connector disconnected with message queue")
	return nil
}

// Send data to specific topic
func (connector *MQConnector) Send(topic string, data []byte) error {
	err := connector.conn.Send(topic, "application/json", data)
	if err != nil {
		log.Error("send message failed", "error", err, "topic", topic, "data", data)
		return errors.Wrap(err, "send message failed")
	}
	return nil
}

// Subscribe a non-durable topic, return a subscription instance that can receive any update using a built-in channel C
func (connector *MQConnector) Subscribe(topic string, opts ...func(*frame.Frame) error) (*stomp.Subscription, error) {
	if _, ok := connector.subscriptions[topic]; ok {
		return nil, errors.New(fmt.Sprintf("topic:%s already subscribed", topic))
	}
	if newSubscription, err := connector.conn.Subscribe(topic, stomp.AckAuto, opts...); err != nil {
		return nil, errors.Wrap(err, "subscribe failed")
	} else {
		connector.subscriptions[topic] = constants.NON_DURABLE
		return newSubscription, nil
	}
}

// DurableSubscribe subscribe a durable topic
func (connector *MQConnector) DurableSubscribe(topic string, ackMode stomp.AckMode, opts ...func(*frame.Frame) error) (*stomp.Subscription, error) {
	opts = append(opts, stomp.SubscribeOpt.Header("activemq.subscriptionName", topic))
	if _, ok := connector.subscriptions[topic]; ok {
		return nil, errors.New(fmt.Sprintf("topic:%s subscribed", topic))
	}
	if newSubscription, err := connector.conn.Subscribe(topic, ackMode, opts...); err != nil {
		return nil, errors.Wrap(err, "durable subscribe failed")
	} else {
		connector.subscriptions[topic] = constants.DURABLE
		return newSubscription, nil
	}
}

// Unsubscribe a topic
func (connector *MQConnector) Unsubscribe(topic string) error {
	if _, ok := connector.subscriptions[topic]; !ok {
		return errors.New(fmt.Sprintf("topic:%s is not subscriped", topic))
	} else {
		delete(connector.subscriptions, topic)
		return nil
	}
}

// Ack to message queue when received any message
func (connector *MQConnector) Ack(m *stomp.Message) error {
	if err := connector.conn.Ack(m); err != nil {
		log.Error("ack failed", "error", err)
		return errors.Wrap(err, "ack failed")
	}
	return nil
}

// Reconnect to mq and resubscribe to topics
func (connector *MQConnector) Reconnect() error {
	connectionTime := 0
	if err := connector.disconnect(); err == nil {
		for err := connector.connect(connector.heartBeatTime); err != nil; err = connector.connect(connector.heartBeatTime) {
			time.Sleep(1 * time.Second)
			connectionTime++
			if connectionTime >= 10 {
				log.Error("reconnect too many times, shut down", "client-id", connector.clientID, "error", err)
				return errors.New(fmt.Sprintf("reconnect too many times, shut down, client-id:%s", connector.clientID))
			}
		}
		connector.subscriptions = map[string]int{} // clear older subscriptions
	} else {
		log.Error("cannot disconnect", "client-id", connector.clientID, "error", err)
		return errors.Wrap(err, "cannot disconnect")
	}
	return nil
}
