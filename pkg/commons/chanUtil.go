package commons

import "errors"

// safeCloseBytesChan close a channel safely
// return true if channel is closed by this call
func SafeCloseBytesChan(ch chan []byte) (justClosed bool) {
	defer func() {
		if recover() != nil {
			// The return result can be altered
			// in a defer function call.
			justClosed = false
		}
	}()

	// assume ch != nil here.
	close(ch)   // panic if ch is closed
	return true // <=> justClosed = true; return
}

// safeSendBytesChan send data to chan safely
// returns true if chan is closed
func SafeSendBytesChan(ch chan []byte, value []byte) (err error) {
	defer func() {
		if recover() != nil {
			err = errors.New("message sent to a closed chan")
		}
	}()

	ch <- value // panic if ch is closed
	return nil  // <=> closed = false; return
}

// safeCloseBoolChan close a channel safely
// return true if channel is closed by this call
func SafeCloseBoolChan(ch chan bool) (justClosed bool) {
	defer func() {
		if recover() != nil {
			// The return result can be altered
			// in a defer function call.
			justClosed = false
		}
	}()

	// assume ch != nil here.
	close(ch)   // panic if ch is closed
	return true // <=> justClosed = true; return
}

// safeSendBoolChan send data to chan safely
// returns true if chan is closed
func SafeSendBoolChan(ch chan bool, value bool) (err error) {
	defer func() {
		if recover() != nil {
			err = errors.New("message sent to a closed chan")
		}
	}()

	ch <- value // panic if ch is closed
	return nil  // <=> closed = false; return
}

// safeCloseIntChan close a channel safely
// return true if channel is closed by this call
func SafeCloseIntChan(ch chan int) (justClosed bool) {
	defer func() {
		if recover() != nil {
			// The return result can be altered
			// in a defer function call.
			justClosed = false
		}
	}()

	// assume ch != nil here.
	close(ch)   // panic if ch is closed
	return true // <=> justClosed = true; return
}

// safeSendIntChan send data to chan safely
// returns true if chan is closed
func SafeSendIntChan(ch chan int, value int) (err error) {
	defer func() {
		if recover() != nil {
			err = errors.New("message sent to a closed chan")
		}
	}()

	ch <- value // panic if ch is closed
	return nil  // <=> closed = false; return
}
