package writer

import (
	"errors"
)

// Writer is the interface of writer define the following functions
type Writer interface {
	Write(interface{}) error
	writeBytes([]byte) error
}

// NewWriter returns a new writer instance
func NewWriter(conf *Config) (Writer, error) {
	switch conf.Type {
	case "mongo":
		return NewMongoWriter(conf)
	case "kinesis":
		return NewKinesisWriter(conf)
	case "firehose":
		return NewFirehoseWriter(conf)
	default:
		return nil, errors.New("conf type not supported")
	}
}
