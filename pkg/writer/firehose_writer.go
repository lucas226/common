package writer

import (
	"encoding/json"
	"fmt"
	"sync"

	"bitbucket.org/lucas226/common/pkg/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/firehose"
	"github.com/pkg/errors"
)

const newLineChar = byte('\n')
const fireHoseMaxAcceptDataLength = 1024000

var firehoseSession *session.Session

var firehoseClient *firehose.Firehose

// FirehoseWriter implements firehose writer
type FirehoseWriter struct {
	StreamName    string
	MinRecordSize int
	MaxRecordSize int
	buffer        []byte
	bufferPtr     int
	mutex         *sync.Mutex
}

// NewFirehoseWriter returns a FirehoseWriter
// return error if the specified stream is not present in AWS
func NewFirehoseWriter(cfg *Config) (*FirehoseWriter, error) {
	if firehoseSession == nil {
		firehoseSession = session.Must(session.NewSession())
	}
	if firehoseClient == nil {
		firehoseClient = firehose.New(firehoseSession, aws.NewConfig().WithRegion(cfg.Region))
	}

	streamInfo, err := firehoseClient.DescribeDeliveryStream(&firehose.DescribeDeliveryStreamInput{
		DeliveryStreamName: &cfg.StreamName,
	})

	if err != nil {
		log.Error("firehose describe stream failed: ", err)
		return nil, err
	}

	if *streamInfo.DeliveryStreamDescription.DeliveryStreamStatus != "ACTIVE" {
		log.Error("firehose stream not ready: ", streamInfo)
		return nil, errors.New("firehose stream not ready")
	}

	fhw := &FirehoseWriter{
		StreamName:    cfg.StreamName,
		MinRecordSize: cfg.MinRecordSize,
		MaxRecordSize: cfg.MaxRecordSize,
		mutex:         &sync.Mutex{},
	}

	return fhw, nil
}

// Write serializes msg into JSON and then writes
func (w *FirehoseWriter) Write(msg interface{}) error {
	var data []byte
	// serialize msg if it's not []byte
	switch v := msg.(type) {
	case []byte:
		data = v
	case string:
		data = []byte(v)
	default:
		d, err := json.Marshal(msg)
		if err != nil {
			log.Error("cannot marshal msg", "error", err, "message", msg)
			return err
		}
		data = d
	}

	// append newline char to data
	newData := append(data, newLineChar)

	w.mutex.Lock()
	if len(w.buffer)+len(newData) > w.MaxRecordSize {
		go w.writeBytes(w.buffer)
		w.buffer = newData
	} else {
		w.buffer = append(w.buffer, newData...)
	}

	if len(w.buffer) > w.MinRecordSize {
		go w.writeBytes(w.buffer)
		w.buffer = []byte{}
	}
	w.mutex.Unlock()

	return nil
}

// WriteBytes write bytes as a single message and apppends newline char at the end of the message
func (w *FirehoseWriter) writeBytes(data []byte) error {
	// return error before put data to avoid firehose performance issue.
	if len(data) > fireHoseMaxAcceptDataLength {
		msg := fmt.Sprintf("invalid data length. max is %d, actual data length is %d. data %s", fireHoseMaxAcceptDataLength, len(data), data)
		// output to logger
		// because this error will be ignored in most of cases.
		log.Error("cannot put record", "error", msg)
		return errors.New(msg)
	}

	_, err := firehoseClient.PutRecord(&firehose.PutRecordInput{
		DeliveryStreamName: &w.StreamName,
		Record:             &firehose.Record{Data: data},
	})
	if err != nil {
		log.Error("cannot put record", "error", err, "data", data)
		return err
	}
	log.Debug("putRecord success", "size", len(data))
	return nil
}

// WriteBytes write bytes as a single message and apppends newline char at the end of the message
func (w *FirehoseWriter) writeBytesByBatch(dataArray [][]byte) error {
	var records []*firehose.Record
	for _, data := range dataArray {
		// append newline char to data
		records = append(records, &firehose.Record{Data: data})
	}

	_, err := firehoseClient.PutRecordBatch(&firehose.PutRecordBatchInput{
		DeliveryStreamName: &w.StreamName,
		Records:            records,
	})

	if err != nil {
		log.Error("cannot put records", "error", err)
		return err
	}
	log.Debug("putRecords", "size", len(dataArray))
	return nil
}
