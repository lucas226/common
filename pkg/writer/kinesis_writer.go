package writer

import (
	"encoding/json"
	"fmt"
	"sync"

	"bitbucket.org/lucas226/common/pkg/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/pkg/errors"
	"github.com/rs/xid"
)

var kinesisSession *session.Session

// Create a kinesis client from just a session.
var kinesisClient *kinesis.Kinesis

// KinesisWriter implements kinesis writer
type KinesisWriter struct {
	StreamName    string
	MinRecordSize int
	MaxRecordSize int
	buffer        []byte
	mutex         *sync.Mutex
}

// NewKinesisWriter returns a KinesisWriter
// if the specified stream is not present in AWS returns error
func NewKinesisWriter(cfg *Config) (*KinesisWriter, error) {
	if kinesisSession == nil {
		kinesisSession = session.Must(session.NewSession())
	}
	if kinesisClient == nil {
		kinesisClient = kinesis.New(kinesisSession, aws.NewConfig().WithRegion("us-west-2"))
	}

	streamInfo, err := kinesisClient.DescribeStream(&kinesis.DescribeStreamInput{
		StreamName: &cfg.StreamName,
	})

	if err != nil {
		log.Error("firehose describe stream failed: ", err)
		return nil, err
	}

	if *streamInfo.StreamDescription.StreamStatus != "ACTIVE" {
		log.Error("kinesis stream not ready: ", streamInfo)
		return nil, errors.New("kinesis stream not ready")
	}

	kw := &KinesisWriter{
		StreamName:    cfg.StreamName,
		MinRecordSize: cfg.MinRecordSize,
		MaxRecordSize: cfg.MaxRecordSize,
		buffer:        []byte{},
		mutex:         &sync.Mutex{},
	}

	return kw, nil
}

// Write serializes msg into JSON and then writes
func (w *KinesisWriter) Write(msg interface{}) error {
	var data []byte
	// serialize msg if it's not []byte
	switch v := msg.(type) {
	case []byte:
		data = v
	case string:
		data = []byte(v)
	default:
		d, err := json.Marshal(msg)
		if err != nil {
			log.Error("cannot marshal msg", "error", err, "message", msg)
			return err
		}
		data = d
	}
	// append newline char to data
	newData := append(data, newLineChar)

	w.mutex.Lock()
	if len(w.buffer)+len(newData) > w.MaxRecordSize {
		go w.writeBytes(w.buffer)
		w.buffer = newData
	} else {
		w.buffer = append(w.buffer, newData...)
	}

	if len(w.buffer) > w.MinRecordSize {
		go w.writeBytes(w.buffer)
		w.buffer = []byte{}
	}
	w.mutex.Unlock()

	return nil
}

// WriteBytes write bytes as a single message and apppends newline char at the end of the message
func (w *KinesisWriter) writeBytes(data []byte) error {
	// return error before put data to avoid firehose performance issue.
	if len(data) > fireHoseMaxAcceptDataLength {
		msg := fmt.Sprintf("invalid data length. max is %d, actual data length is %d. data %s", fireHoseMaxAcceptDataLength, len(data), data)
		// output to logger
		// because this error will be ignored in most of cases.
		log.Error("cannot put record", "error", msg)
		return errors.New(msg)
	}

	key := xid.New().String()
	_, err := kinesisClient.PutRecord(&kinesis.PutRecordInput{
		StreamName:   &w.StreamName,
		PartitionKey: &key,
		Data:         data,
	})

	if err != nil {
		log.Error("cannot put record", "error", err, "data", data)
		return err
	}
	log.Debug("putRecord success", "size", len(data))
	return nil
}

// WriteBytes write bytes as a single message and apppends newline char at the end of the message
func (w *KinesisWriter) writeBytesByBatch(dataArray [][]byte) error {
	var records []*kinesis.PutRecordsRequestEntry
	for _, data := range dataArray {
		// append newline char to data
		key := xid.New().String()
		records = append(records, &kinesis.PutRecordsRequestEntry{
			PartitionKey: &key,
			Data:         data,
		})
	}

	_, err := kinesisClient.PutRecords(&kinesis.PutRecordsInput{
		StreamName: &w.StreamName,
		Records:    records,
	})

	if err != nil {
		log.Error("cannot put records", "error", err)
		return err
	}
	log.Debug("putRecords", "size", len(dataArray))
	return nil
}
