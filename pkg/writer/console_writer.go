package writer

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/lucas226/common/pkg/log"
)

// ConsoleWriter define the console writer structure
type ConsoleWriter struct {
	pattern string
}

// Write to write message
func (w *ConsoleWriter) Write(msg interface{}) error {
	data, err := json.Marshal(msg)
	if err != nil {
		log.Info("cannot marshal msg", "msg", msg, "error", err)
		return err
	}

	return w.writeBytes(data)
}

func (w *ConsoleWriter) writeBytes(data []byte) error {
	fmt.Printf(w.pattern, data)
	return nil
}
