package writer

// Config is config for writer
type Config struct {
	Type string // type is one of "console", "mongo", "kinesis", "firehose"
	// for mongo writer
	ConnectionURL string
	Database      string
	Collection    string

	// for Kinesis & Firehose writer
	Region        string
	StreamName    string `json:"stream_name"`
	MinRecordSize int    `json:"min_record_size"`
	MaxRecordSize int    `json:"max_record_size"`
}
