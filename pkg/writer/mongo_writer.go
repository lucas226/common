package writer

import (
	"encoding/json"

	"bitbucket.org/lucas226/common/pkg/log"
	"github.com/globalsign/mgo"
)

// MongoWriter define the mongo writer structure
type MongoWriter struct {
	Name       string
	Collection *mgo.Collection
}

// NewMongoWriter return the new instance of mongo writer
func NewMongoWriter(cfg *Config) (*MongoWriter, error) {
	session, err := mgo.Dial(cfg.ConnectionURL)
	if err != nil {
		return nil, err
	}
	coll := session.DB(cfg.Database).C(cfg.Collection)
	w := &MongoWriter{
		Name:       cfg.Collection,
		Collection: coll,
	}
	return w, nil
}

func (w *MongoWriter) Write(msg interface{}) error {
	// Just for validate.
	_, err := json.Marshal(msg)
	if err != nil {
		log.Info("cannot marshal msg: ", msg, err)
		return err
	}
	return w.Collection.Insert(msg)
}

// Does not use, but exist for interface
func (w *MongoWriter) writeBytes(data []byte) error {
	return nil
}
