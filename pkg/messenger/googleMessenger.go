package messenger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

type GoogleMessenger struct {
	httpClient http.Client // http client for get and post
	targetUrl  string      // url that messenger should send message to
	meta       messengerMetaData
}

func NewGoogleMessenger(url string, meta messengerMetaData) *GoogleMessenger {
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	return &GoogleMessenger{
		httpClient: client,
		targetUrl:  url,
		meta:       meta,
	}
}

func (gm *GoogleMessenger) About() string {
	return "Google Messenger"
}

// SendAsync sends message to google
func (gm *GoogleMessenger) SendAsync(msg interface{}) {
	go gm.Send(msg)
}

// Send message to messenger
func (gm *GoogleMessenger) Send(msg interface{}) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return errors.New(fmt.Sprintf("Cannot Send, marshal message error:%v", err))
	}
	marshalMsg, err := json.Marshal(map[string]string{
		"text": fmt.Sprintf("%s\nInstance Base Info: ip: %+v, instanceId: %+v", b, gm.meta.ip, gm.meta.instanceID),
	})
	if err != nil {
		return errors.New(fmt.Sprintf("Cannot Send, marshal message error:%v", err))
	}
	// post message to google chat
	if resp, err := gm.httpClient.Post(gm.targetUrl, "application/json", bytes.NewBuffer(marshalMsg)); err != nil {
		return errors.New(fmt.Sprintf("send message to google chat failed:%v", err))
	} else {
		if resp.StatusCode != 200 {
			return errors.New(fmt.Sprintf("send message to google chat failed: %v", resp))
		}
	}
	return nil
}
