package messenger

import (
	"fmt"
	"testing"
)

func TestMessenger(t *testing.T) {
	configs := []MessengerConfig{
		MessengerConfig{
			Service: "google",
			URL:     "https://chat.googleapis.com/v1/spaces/AAAA_9uKXz0/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=pIvwcfldEhyAOIf0Z0fKZGK5iAmQcfkvsUs0Peloq8c%3D",
		},
	}

	if err := RegisterMessengers(configs); err != nil {
		panic(err)
	}

	for i := 0; i < 3; i++ {
		Send(fmt.Sprintf("test %d", i))
	}
}
