package messenger

import (
	"fmt"
	"net"

	"bitbucket.org/lucas226/common/pkg/log"
	externalip "github.com/GlenDC/go-external-ip"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

var messengers []Messenger

// Messenger is the unified messenger interface
type Messenger interface {
	About() string
	Send(interface{}) error
	SendAsync(interface{})
}

type MessengerConfig struct {
	Service string // "google", "slack" ...
	URL     string // url
}

type messengerMetaData struct {
	ip         net.IP // new instance ip
	instanceID string // instance uuid
}

func RegisterMessengers(configs []MessengerConfig) error {
	for _, cfg := range configs {
		m, err := newMessenger(cfg)
		if err != nil {
			return err
		}
		messengers = append(messengers, m)
	}
	return nil
}

func Send(msg interface{}) error {
	hasError := false
	for _, m := range messengers {
		if err := m.Send(msg); err != nil {
			log.Error("failed to send message.",
				"error", err.Error(),
				"messenger", m.About(),
			)
			hasError = true
		}
	}
	if hasError {
		return errors.New("failed to send message")
	}
	return nil
}

func SendAsync(msg interface{}) {
	go Send(msg)
}

// NewMessenger returns a new messenger instance
func newMessenger(config MessengerConfig) (Messenger, error) {
	consensus := externalip.DefaultConsensus(nil, nil)
	ip, err := consensus.ExternalIP()
	if err != nil {
		return nil, errors.Wrap(err, "get ip failed")
	}
	meta := messengerMetaData{
		ip:         ip,
		instanceID: uuid.New().String(),
	}

	switch config.Service { // more types of messenger may be added later
	case "google":
		messenger := NewGoogleMessenger(config.URL, meta)
		return messenger, nil
	default:
		return nil, errors.New(fmt.Sprintf("messenger service: %s is not supported", config.Service))
	}
}
