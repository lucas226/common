package constants

/*************************************************** Channel Name ********************************************************/
// tt channel name
const (
	TT_ORDERBOOK_CHANNEL string = "orderbook"
	TT_TICKER_CHANNEL    string = "ticker"
	TT_TRADE_CHANNEL     string = "trade"
)

// binance channel name
const (
	BINANCE_ORDERBOOK_CHANNEL string = "depth"
	BINANCE_TICKER_CHANNEL    string = "ticker"
	BINANCE_TRADE_CHANNEL     string = "trade"
)

// bitfinex channel name
const (
	BITFINEX_ORDERBOOK_CHANNEL string = "book"
	BITFINEX_TICKER_CHANNEL    string = "ticker"
	BITFINEX_TRADE_CHANNEL     string = "trades"
)

// bitflyer channel name
const (
	BITFLYER_ORDERBOOK_SNAPSHOT_CHANNEL string = "board_snapshot"
	BITFLYER_ORDERBOOK_UPDATE_CHANNEL   string = "board"
	BITFLYER_TICKER_CHANNEL             string = "ticker"
	BITFLYER_TRADE_CHANNEL              string = "executions"
)

// bitmex channel name
const (
	BITMEX_ORDERBOOK_CHANNEL string = "orderBookL2"
	BITMEX_TICKER_CHANNEL    string = "quote"
	BITMEX_TRADE_CHANNEL     string = "trade"
)

// coinbasePro channel name
const (
	COINBASEPRO_ORDERBOOK_CHANNEL string = "level2"
	COINBASEPRO_TICKER_CHANNEL    string = "ticker"
	COINBASEPRO_TRADE_CHANNEL     string = "matches"
)

// huobi channel name
const (
	HUOBI_ORDERBOOK_CHANNEL string = "depth.step0"
	HUOBI_TICKER_CHANNEL    string = "detail"
	HUOBI_TRADE_CHANNEL     string = "trade.detail"
)

// okex channel name
const (
	OKEX_ORDERBOOK_CHANNEL string = "depth"
	OKEX_TICKER_CHANNEL    string = "ticker"
	OKEX_TRADE_CHANNEL     string = "trade"
)

// deribit channel name
const (
	DERIBIT_ORDERBOOK_CHANNEL string = "book"
	DERIBIT_TICKER_CHANNEL    string = "ticker"
	DERIBIT_TRADE_CHANNEL     string = "trades"
)

// coinapi channel name
const (
	COINAPI_ORDERBOOK_CHANNEL string = "book"
	COINAPI_TRADE_CHANNEL     string = "trade"
)
