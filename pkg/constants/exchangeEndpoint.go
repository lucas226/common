package constants

/*************************************************** Websocket Endpoint ****************************************************/
const (
	BITMEX_WS_ENDPOINT      string = "wss://www.bitmex.com/realtime"
	COINBASEPRO_WS_ENDPOINT string = "wss://ws-feed.pro.coinbase.com"
	BITFINEX_WS_ENDPOINT    string = "wss://api.bitfinex.com/ws"
	BITFLYER_WS_ENDPOINT    string = "wss://ws.lightstream.bitflyer.com/json-rpc"
	OKEX_WS_ENDPOINT        string = "wss://real.okex.com:10442/ws/v3"
	BINANCE_WS_ENDPOINT     string = "wss://stream.binance.com:9443"
	HUOBI_WS_ENDPOINT       string = "wss://api.huobi.pro/ws/"
	COINAPI_WS_ENDPOINT     string = "wss://ws.coinapi.io/v1/"
	DERIBIT_WS_ENDPOINT     string = "wss://www.deribit.com/ws/api/v2"
)

/*************************************************** REST Endpoint ********************************************************/
const (
	BINANCE_REST_ENDPOINT  string = "https://www.binance.com/api/v1/"
	BITFLYER_REST_ENDPOINT string = "https://api.bitflyer.com/v1/"
)
