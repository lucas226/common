package constants

/****************** subscription type **********/
const (
	NON_DURABLE = iota
	DURABLE
)
