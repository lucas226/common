package constants

/************************************************** Exchange Name **************************************************/
const (
	BITMEX      string = "BITMEX"
	BITFINEX    string = "BITFINEX"
	HUOBI       string = "HUOBI"
	OKEX        string = "OKEX"
	BITFLYER    string = "BITFLYER"
	COINBASEPRO string = "COINBASEPRO"
	BINANCE     string = "BINANCE"
	DERIBIT     string = "DERIBIT"
	COINAPI     string = "COINAPI" //note: coinapi is not an exchange name, but a cross-exchange data platform
)
