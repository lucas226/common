package constants

/******************************** TT definition of Symbol Security Type ****************************************************/
const (
	SPOT      string = "SPOT"
	FUTURES   string = "FTS"
	PERPETUAL string = "PERP"
	OPTION    string = "OPT" // currently not useful for tt
	INDEX     string = "IDX" // currently not useful for tt
)
