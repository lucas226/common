package constants

/********************************************* Market Info Message Event Type ******************************************/
// tt event types
const (
	TT_ORDERBOOK_SNAPSHOT string = "orderbookSnapshot"
	TT_ORDERBOOK_UPDATE   string = "orderboookUpdate"
	TT_TICKER_UPDATE      string = "tickerUpdate"
	TT_TRADE_UPDATE       string = "tradeUpdate"
)

// binance event type
const (
	BINANCE_ORDERBOOK_SNAPSHOT string = "book"
	BINANCE_ORDERBOOK_UPDATE   string = "depthUpdate"
	BINANCE_TICKER_UPDATE      string = "24hrTicker"
	BINANCE_TRADE_UPDATE       string = "trade"
)

// bitfinex event type
const (
	BITFINEX_ORDERBOOK_SNAPSHOT string = "orderbookSnapshot"
	BITFINEX_ORDERBOOK_UPDATE   string = "orderbookUpdate"
	BITFINEX_TICKER_UPDATE      string = "tickerUpdate"
	BITFINEX_TRADE_UPDATE       string = "tradeUpdate"
)

// bitflyer event type
const (
	BITFLYER_ORDERBOOK_SNAPSHOT string = "snapshot"
	BITFLYER_ORDERBOOK_UPDATE   string = "board"
	BITFLYER_TICKER_UPDATE      string = "ticker"
	BITFLYER_TRADE_UPDATE       string = "executions"
)

// bitmex event type
const (
	BITMEX_ORDERBOOK_SNAPSHOT string = "orderBookL2partial"
	BITMEX_ORDERBOOK_UPDATE   string = "orderBookL2update"
	BITMEX_ORDERBOOK_INSERT   string = "orderBookL2insert"
	BITMEX_ORDERBOOK_DELETE   string = "orderBookL2delete"

	BITMEX_TICKER_SNAPSHOT string = "quotepartial"
	BITMEX_TICKER_UPDATE   string = "quoteupdate"
	BITMEX_TICKER_INSERT   string = "quoteinsert"
	BITMEX_TICKER_DELETE   string = "quotedelete"

	BITMEX_TRADE_SNAPSHOT string = "tradepartial"
	BITMEX_TRADE_UPDATE   string = "tradeupdate"
	BITMEX_TRADE_INSERT   string = "tradeinsert"
	BITMEX_TRADE_DELETE   string = "tradedelete"
)

// coinbasePro event type
const (
	COINBASEPRO_ORDERBOOK_SNAPSHOT string = "snapshot"
	COINBASEPRO_ORDERBOOK_UPDATE   string = "l2update"
	COINBASEPRO_TICKER_UPDATE      string = "ticker"
	COINBASEPRO_TRADE_UPDATE       string = "match"
	COINBASEPRO_LAST_TRADE         string = "last_match"
)

// huobi event type
const (
	HUOBI_ORDERBOOK_SNAPSHOT string = "depth"
	HUOBI_TICKER_UPDATE      string = "detail"
	HUOBI_TRADE_UPDATE       string = "trade"
)

// okex event type
const (
	OKEX_ORDERBOOK_SNAPSHOT string = "partial"
	OKEX_ORDERBOOK_UPDATE   string = "update"
	OKEX_TICKER_UPDATE      string = "ticker"
	OKEX_TRADE_UPDATE       string = "trade"
)

// deribit event type
const (
	DERIBIT_ORDERBOOK_SNAPSHOT string = "snapshot"
	DERIBIT_ORDERBOOK_NEW      string = "booknew"
	DERIBIT_ORDERBOOK_CHANGE   string = "bookchange"
	DERIBIT_ORDERBOOK_DELETE   string = "bookdelete"
	DERIBIT_TRADES             string = "trades"
)
